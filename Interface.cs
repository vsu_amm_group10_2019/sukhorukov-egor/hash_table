﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
        interface IHashTable
    {
        bool Add(Employee employee);
        Employee Find(string id);
        void Delete(string id);
        void Clear();
        List<Employee> GetAllData();
    }
}
