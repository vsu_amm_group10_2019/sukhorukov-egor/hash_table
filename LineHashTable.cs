﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
        public class LineHashTable:IHashTable
    {
        private Elem[] Table { get; }
        public int Size { get; }
        public LineHashTable(int size = 100) // Конструктор
        {
            Size = size;
            Table = new Elem[size];
        }
        public bool Add(Employee employee)
        {
            if (Find(employee.ID) != null)
            {
                return false;
            }
            // Добавление в таблицу
            int hash = Employee.Hash(employee.ID);
            int index = hash % Size;
            if (Table[index] == null || Table[index].Deleted) 
            {
                Table[index] = new Elem()
                {
                    Employee = employee
                };
                return true;
            }
            // Метод решении коллизии
            int hash_collision = Employee.Hash_Collision(employee.ID);
            for (int i = 1; i <= Size; i++)
            {
                int index_collision = (hash_collision * i + hash) % Size;
                if (Table[index_collision] == null || Table[index_collision].Deleted)
                {
                    Table[index_collision] = new Elem()
                    {
                        Employee = employee
                    };
                    return true;
                }
            }
            return false;
        }
        public Employee Find(string id)
        {
            int hash = Employee.Hash(id);
            int index = hash % Size;
            if (Table[index] == null)
            {
                return null; 
            }
            if (!Table[index].Deleted && Table[index].Employee.ID == id)
            {
                return Table[index].Employee;
            }
            int hashCollision = Employee.Hash_Collision(id);
            for (int i = 1; i <= Size; i++)
            {    
                int indexCollision = (hashCollision * i + hash) % Size;
                if (Table[indexCollision] == null)
                {
                    return null;
                }
                if (!Table[indexCollision].Deleted && Table[indexCollision].Employee.ID == id)
                {
                    return Table[indexCollision].Employee;
                }
            }
            return null;
        }
        public void Delete (string id)
        {
            int hash = Employee.Hash(id);
            int index = hash % Size;
            if (Table[index] == null)
            {
                return;
            }
            if (!Table[index].Deleted && Table[index].Employee.ID == id)
            {
                Table[index].Deleted = true;
                return;
            }
            int hashCollision = Employee.Hash_Collision(id);
            for (int i = 1; i <= Size; i++)
            {
                int indexCollision = (hashCollision * i + hash) % Size;
                if (Table[indexCollision] == null)
                {
                    return;
                }
                if (!Table[indexCollision].Deleted && Table[indexCollision].Employee.ID == id)
                {
                    return;
                }
            }
        }
        public void Clear()
        {
            for (int i = 0; i < Size; i++)
            {
                Table[i] = null;
            }
        }

        public List<Employee> GetAllData()
        {
            List<Employee> result = new List<Employee>();
            foreach (Elem elem in Table){
                if (elem != null && !elem.Deleted)
                {
                    result.Add(elem.Employee);
                } 
            }
            return result;
        }
    }
    class Elem
    {
        public Employee Employee { get; set; }
        public bool Deleted { get; set; } = false;

    }
    
}
