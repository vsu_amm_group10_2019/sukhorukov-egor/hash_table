﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConsoleApp1;

namespace HashTable
{
    static class MainProgram
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
            

    }
}
}
/*static void Main(string[] args)
{
    IHashTable table = new LineHashTable(50);
    table.Add(new Student()
    {
        ID = "123412",
        FIO = Guid.NewGuid().ToString(),
        Age = new DateTime(2001, 02, 15),
        Course = 1,
        Group = "10",
    }
    );
    table.Add(new Student()
    {
        ID = "411412",
        FIO = Guid.NewGuid().ToString(),
        Age = new DateTime(2000, 12, 11),
        Course = 2,
        Group = "12",
    }
    );
    table.Add(new Student()
    {
        ID = "123612",
        FIO = Guid.NewGuid().ToString(),
        Age = new DateTime(2002, 01, 10),
        Course = 4,
        Group = "23",
    }
    );
    Student student = table.Find("411412");
    Console.WriteLine(JsonConvert.SerializeObject(student));
    // table.Delete(student.ID);

}*/