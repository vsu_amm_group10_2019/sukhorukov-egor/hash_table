﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Employee
    {
        public string ID { get; set; }
        public string FIO { get; set; }
        public double Salary { get; set; }
        public static int Hash(string id)
        {
            int result = 0;
            foreach (char c in id)
            {
                result += c;
            }
            return result;
        }
        public static int Hash_Collision(string id)
        {
            int result = 0;
            int index = 1;
            foreach (char c in id)
            {
                result += c+index;
                index++;
            }
            return result;
        }
    }
}
