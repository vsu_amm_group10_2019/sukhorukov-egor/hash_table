﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConsoleApp1;

namespace HashTable
{
    public partial class AddForm : Form
    {
        FormState FormState;
        public AddForm(FormState formstate, Employee employee = null)
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            FormState = formstate;
            ok = false;
            switch (formstate)
            {
                case FormState.ADD:
                    {
                        return;
                    }
                case FormState.EDIT:
                    {
                        Text = "Изменить";
                        textBoxId.ReadOnly = true;
                        textBoxId.Text = employee.ID;
                        textBoxFio.Text = employee.FIO;
                        textBoxSalary.Text = Convert.ToString(employee.Salary);
                        break;
                    }
                case FormState.DELETE:
                    {
                        Text = "Поиск";
                        textBoxFio.ReadOnly = true;
                        textBoxSalary.ReadOnly = true;
                        break;
                    }
                case FormState.SEARCH:
                    {
                        Text = "Поиск";
                        textBoxFio.ReadOnly = true;
                        textBoxSalary.ReadOnly = true;
                        break;
                    }
                case FormState.DISPLAY:
                    {
                        Text = "Сотрудник";
                        textBoxId.ReadOnly = true;
                        textBoxFio.ReadOnly = true;
                        textBoxSalary.ReadOnly = true;
                        textBoxId.Text = employee.ID;
                        textBoxFio.Text = employee.FIO;
                        textBoxSalary.Text = Convert.ToString(employee.Salary);
                        break;
                    }
            }
        }
        public Employee Employee { get; } = new Employee();
        public bool ok;
        private void buttonAddOk_Click(object sender, EventArgs e)
        {
            if  (FormState == FormState.ADD || FormState == FormState.EDIT)
            {
                if (textBoxId.Text != "" && textBoxFio.Text != "" && textBoxSalary.Text != "")
                {
                    Employee.ID = textBoxId.Text;
                    Employee.FIO = textBoxFio.Text;
                    try
                    {
                        Employee.Salary = Convert.ToDouble(textBoxSalary.Text);
                        ok = true;
                        this.Close();
                    }
                    catch
                    {
                        MessageBox.Show(
                            "Поле зарплаты заполнено не верно!",
                            "Ошибка ввода!",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    }
                }
                else
                    MessageBox.Show(
                            "Заполнены не все поля!",
                            "Ошибка ввода!",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
            }

            if (FormState == FormState.DELETE || FormState == FormState.SEARCH)
            {
                if (textBoxId.Text != "")
                {
                    Employee.ID = textBoxId.Text;
                    ok = true;
                    this.Close();
                }
                else
                    MessageBox.Show(
                            "Поле 'id' не заполнено!",
                            "Ошибка ввода!",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
            }
            if (FormState == FormState.DISPLAY)
            {
                ok = true;
                this.Close();
            }
        }
    }
}
