﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConsoleApp1;
using Newtonsoft.Json;

namespace HashTable
{
    
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private string FileName { get; set; } = null;
        LineHashTable table = new LineHashTable(50);
        
        public void Redraw()
        {
            List<Employee> employees = table.GetAllData();
            dataGridView.Rows.Clear();
            dataGridView.RowCount = employees.Count;
            for (int i=0; i<employees.Count; i++)
            {
                dataGridView.Rows[i].Cells[0].Value = employees[i].ID;
                dataGridView.Rows[i].Cells[1].Value = employees[i].FIO;
                dataGridView.Rows[i].Cells[2].Value = employees[i].Salary;
            }
        }

        private void menuAdd_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm(FormState.ADD);
            addForm.ShowDialog();
            if (addForm.ok)
            {
                if (table.Add(addForm.Employee))
                {
                    Redraw();
                }
                else
                {
                    MessageBox.Show("Не удалось добавить запись");
                }
            }
        }

        private void menuSearch_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.SEARCH);
            search.ShowDialog();
            if (search.ok)
            {
                string id = search.Employee.ID;
                Employee employee = table.Find(id);
                if (employee != null)
                {
                    AddForm show = new AddForm(FormState.DISPLAY, employee);
                    show.ShowDialog();

                }
                else
                {
                    MessageBox.Show("Сотрудник не найден");
                }
            }
            
        }

        private void menuEdit_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.SEARCH);
            search.ShowDialog();
            if (search.ok)
            {
                string id = search.Employee.ID;
                Employee employee = table.Find(id);
                if (employee != null)
                {
                    AddForm edit = new AddForm(FormState.EDIT, employee);
                    edit.ShowDialog();
                    table.Delete(id);
                    if (table.Add(edit.Employee))
                    {
                        Redraw();
                    }
                    else
                    {
                        MessageBox.Show("Не удалось добавить запись");
                    }

                }
                else
                {
                    MessageBox.Show("Сотрудник не найден");
                }
            }            
        }

        private void menuDelete_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.DELETE);
            search.ShowDialog();
            if (search.ok)
            {
                string id = search.Employee.ID;
                table.Delete(id);
                Redraw();
            }
        }

        private void menuOpen_Click(object sender, EventArgs e)
        {
            
            if (!string.IsNullOrEmpty(FileName))
            {
                DialogResult result = MessageBox.Show(
                    "Сохранить открытый файл?",
                    "Открыт другой файл",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly);
                if (result == DialogResult.Yes)
                {
                    SaveToFile();
                }
                
            }
            openFileDialog.Filter = "Json File|*.json";
            openFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(openFileDialog.FileName))
            {
                FileName = openFileDialog.FileName;
                string txt = File.ReadAllText(FileName);
                List<Employee> employees = JsonConvert.DeserializeObject<List<Employee>>(txt);
                table.Clear();
                foreach (Employee employee in employees)
                {
                    table.Add(employee);
                }
                Redraw();
            }
        }

        private void menuSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                SaveToFile();
            }
            else
            {
                menuSaveAs_Click(sender, e);
            }
        }

        private void menuNewFile_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                DialogResult result = MessageBox.Show(
                    "Сохранить открытый файл?",
                    "Открыт другой файл",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly);
                if (result == DialogResult.Yes)
                {
                    SaveToFile();
                }

            }
            FileName = "";
            table.Clear();
            Redraw();
        }
        private void SaveToFile()
        {
            List<Employee> employees = table.GetAllData();
            string result = JsonConvert.SerializeObject(employees);
            File.WriteAllText(FileName, result);
        }

        private void menuSaveAs_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Json File|*.json";
            saveFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(saveFileDialog.FileName))
            {
                FileName = saveFileDialog.FileName;
                SaveToFile();
            }
        }

        private void menuExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
